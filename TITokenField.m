//
//  TITokenField.m
//  TITokenField
//
//  Created by Tom Irving on 16/02/2010.
//  Copyright 2012 Tom Irving. All rights reserved.
//

#import "TITokenField.h"
#import "TIToken.h"
#import <QuartzCore/QuartzCore.h>


//==========================================================
#pragma mark - TITokenField -
//==========================================================
#define kTextEmpty @"\u200B" // Zero-Width Space
#define kTextHidden @"\u200D" // Zero-Width Joiner

@interface TITokenFieldInternalDelegate ()
@property (nonatomic, unsafe_unretained) id <UITextFieldDelegate> delegate;
@property (nonatomic, unsafe_unretained) TITokenField * tokenField;
@end

@interface TITokenField ()
{
    CGPoint tokenCaret;
}
@property (nonatomic, retain) TITokenFieldInternalDelegate * internalDelegate;
@property (nonatomic, readonly) CGFloat leftViewWidth;
@property (nonatomic, readonly) CGFloat rightViewWidth;
@property (unsafe_unretained, nonatomic, readonly) UIScrollView * scrollView;
@end

@implementation TITokenField
@synthesize delegate;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Object lifecycle
/////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
		[self setup];
    }
	
    return self;
}

/////////////////////////////////////////////////////////////////////////////////
- (id)initWithCoder:(NSCoder *)aDecoder
{
	
	if ((self = [super initWithCoder:aDecoder]))
    {
		[self setup];
	}
	
	return self;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private
/////////////////////////////////////////////////////////////////////////////////
- (void)setup
{
	
	[self setBorderStyle:UITextBorderStyleNone];
	[self setFont:[UIFont systemFontOfSize:14]];
	[self setBackgroundColor:[UIColor whiteColor]];
	[self setAutocorrectionType:UITextAutocorrectionTypeNo];
	[self setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	
	[self.layer setShadowColor:[[UIColor blackColor] CGColor]];
	[self.layer setShadowOpacity:0.6];
	[self.layer setShadowRadius:12];
	
	[self setText:kTextEmpty];
	
	self.internalDelegate = [[TITokenFieldInternalDelegate alloc] init];
	[self.internalDelegate setTokenField:self];
	[super setDelegate:self.internalDelegate];
	
	_tokens = [[NSMutableArray alloc] init];
	self.editable = YES;
	self.removesTokensOnEndEditing = YES;
	self.tokenizingCharacters = [NSCharacterSet characterSetWithCharactersInString:@","];
    [self layoutTokensAnimated:NO];
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Explicitly implemented properties
/////////////////////////////////////////////////////////////////////////////////
- (void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	[self.layer setShadowPath:[[UIBezierPath bezierPathWithRect:self.bounds] CGPath]];
	[self layoutTokensAnimated:NO];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)setText:(NSString *)text
{
	[super setText:(text.length == 0 ? kTextEmpty : text)];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)setDelegate:(id<TITokenFieldDelegate>)del
{
	delegate = del;
	[self.internalDelegate setDelegate:delegate];
}

/////////////////////////////////////////////////////////////////////////////////
- (NSArray *)tokenTitles
{
	
	NSMutableArray * titles = [[NSMutableArray alloc] init];
	[self.tokens enumerateObjectsUsingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop){[titles addObject:token.title];}];
	return titles;
}

/////////////////////////////////////////////////////////////////////////////////
- (NSArray *)tokenObjects
{
	
	NSMutableArray * objects = [[NSMutableArray alloc] init];
	[self.tokens enumerateObjectsUsingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop){
		[objects addObject:(token.representedObject ? token.representedObject : token.title)];
	}];
	return objects;
}

/////////////////////////////////////////////////////////////////////////////////
- (UIScrollView *)scrollView
{
	return ([self.superview isKindOfClass:[UIScrollView class]] ? (UIScrollView *)self.superview : nil);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Token Handling
/////////////////////////////////////////////////////////////////////////////////
- (TIToken *)addTokenWithTitle:(NSString *)title
{
	return [self addTokenWithTitle:title representedObject:nil];
}

/////////////////////////////////////////////////////////////////////////////////
- (TIToken *)addTokenWithTitle:(NSString *)title representedObject:(id)object
{
	
	if (title.length)
    {
		TIToken * token = [[TIToken alloc] initWithTitle:title representedObject:object font:self.font];
		[self addToken:token];
		return token;
	}
	
	return nil;
}

/////////////////////////////////////////////////////////////////////////////////
- (void)addToken:(TIToken *)token
{
	
	BOOL shouldAdd = YES;
	if ([delegate respondsToSelector:@selector(tokenField:willAddToken:)])
		shouldAdd = [delegate tokenField:self willAddToken:token];
	
	
	if (shouldAdd)
    {
		[self becomeFirstResponder];
		
		[token addTarget:self action:@selector(tokenTouchDown:) forControlEvents:UIControlEventTouchDown];
		[token addTarget:self action:@selector(tokenTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didMoveToken:)];
        [token addGestureRecognizer:panGesture];
        
		[self addSubview:token];
		
		if (![self.tokens containsObject:token]) [self.tokens addObject:token];
		
		if ([delegate respondsToSelector:@selector(tokenField:didAddToken:)])
			[delegate tokenField:self didAddToken:token];
		
		
		[self setResultsModeEnabled:NO];
		[self deselectSelectedToken];
	}
}

/////////////////////////////////////////////////////////////////////////////////
- (void)removeToken:(TIToken *)token
{
	
	if (token == self.selectedToken) [self deselectSelectedToken];
	
	BOOL shouldRemove = YES;
	if ([delegate respondsToSelector:@selector(tokenField:willRemoveToken:)])
		shouldRemove = [delegate tokenField:self willRemoveToken:token];
	
	
	if (shouldRemove)
    {
		
		[token removeFromSuperview];
		[self.tokens removeObject:token];
		
		if ([delegate respondsToSelector:@selector(tokenField:didRemoveToken:)])
			[delegate tokenField:self didRemoveToken:token];
		
		[self setResultsModeEnabled:NO];
	}
}

/////////////////////////////////////////////////////////////////////////////////
- (void)removeAllTokens
{
	[self.tokens enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop) {
		[self removeToken:token];
	}];
	
    [self setText:@""];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)selectToken:(TIToken *)token
{
	
	[self deselectSelectedToken];
	
	_selectedToken = token;
	[self.selectedToken setSelected:YES];
	
	[self becomeFirstResponder];
	[self setText:kTextHidden];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)deselectSelectedToken
{
	
	[self.selectedToken setSelected:NO];
	_selectedToken = nil;
	
	[self setText:kTextEmpty];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)tokenizeText
{
	__block BOOL textChanged = NO;
	
	if (![self.text isEqualToString:kTextEmpty] && ![self.text isEqualToString:kTextHidden]){
		[[self.text componentsSeparatedByCharactersInSet:self.tokenizingCharacters] enumerateObjectsUsingBlock:^(NSString * component, NSUInteger idx, BOOL *stop){
			[self addTokenWithTitle:[component stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			textChanged = YES;
		}];
	}
	
	if (textChanged) [self sendActionsForControlEvents:UIControlEventEditingChanged];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)tokenTouchDown:(TIToken *)token {
	
	if (self.selectedToken != token){
		[self.selectedToken setSelected:NO];
		_selectedToken = nil;
	}
}

/////////////////////////////////////////////////////////////////////////////////
- (void)tokenTouchUpInside:(TIToken *)token
{
	if (self.editable) [self selectToken:token];
}

/////////////////////////////////////////////////////////////////////////////////
- (CGFloat)layoutTokensInternal
{
	CGFloat topMargin = floor(self.font.lineHeight * 4 / 7);
	CGFloat leftMargin = self.leftViewWidth + 12;
	CGFloat hPadding = 8;
	CGFloat rightMargin = self.rightViewWidth + hPadding;
	CGFloat lineHeight = self.font.lineHeight + topMargin + 5;
	
	_numberOfLines = 1;
	tokenCaret = (CGPoint){leftMargin, (topMargin - 1)};
	
	[self.tokens enumerateObjectsUsingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop){
		token.alpha = 1.0f;
		[token setFont:self.font];
		[token setMaxWidth:(self.bounds.size.width - rightMargin - (_numberOfLines > 1 ? hPadding : leftMargin))];
		
		if (token.superview){
			
			if (tokenCaret.x + token.bounds.size.width + rightMargin > self.bounds.size.width){
				_numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
			
			[token setFrame:(CGRect){tokenCaret, token.bounds.size}];
			tokenCaret.x += token.bounds.size.width + 4;
			
			if (self.bounds.size.width - tokenCaret.x - rightMargin < 75){
				_numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
		}
	}];
	
	return tokenCaret.y + lineHeight;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecoginzer
/////////////////////////////////////////////////////////////////////////////////
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

/////////////////////////////////////////////////////////////////////////////////
- (void)didMoveToken:(UIPanGestureRecognizer *)gestureRecognizer
{

    
    UIView *piece = [gestureRecognizer view];
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
    CGPoint newLocation = CGPointMake([piece center].x + translation.x, [piece center].y+translation.y);
    [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y+translation.y)];
    piece.alpha = .7f;
    [gestureRecognizer setTranslation:CGPointZero inView:[piece superview]];
    
    TIToken *tokenBeingMoved = (TIToken *)[gestureRecognizer view];
    int newIndex = 0;

    _numberOfLines = 1;
    CGFloat topMargin = floor(self.font.lineHeight * 4 / 7);
	CGFloat leftMargin = self.leftViewWidth + 12;
	CGFloat hPadding = 8;
	CGFloat rightMargin = self.rightViewWidth + hPadding;
	CGFloat lineHeight = self.font.lineHeight + topMargin + 5;

	
	tokenCaret = (CGPoint){leftMargin, (topMargin - 1)};
    
    NSMutableArray *firstOnEachLine = [[NSMutableArray alloc] init];
    
    
    [self.tokens enumerateObjectsUsingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop){
		[token setFont:self.font];
		[token setMaxWidth:(self.bounds.size.width - rightMargin - (_numberOfLines > 1 ? hPadding : leftMargin))];
		if (token.superview)
        {
			
			if (tokenCaret.x + token.bounds.size.width + rightMargin > self.bounds.size.width)
            {
                [firstOnEachLine addObject:token];
				_numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
			tokenCaret.x += token.bounds.size.width + 4;
			if (self.bounds.size.width - tokenCaret.x - rightMargin < 75)
            {
                [firstOnEachLine addObject:token];
                _numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
		}
	}];
    
    
    
    
    _numberOfLines = 1;
    
	tokenCaret = (CGPoint){leftMargin, (topMargin - 1)};
    for (int i = 0; i < [self.tokens count]; i++)
    {
        TIToken *token = self.tokens[i];
        NSLog(@"%@ - %f > %f && %f < %f && %f > %f", token.title, newLocation.y, token.frame.origin.y + topMargin, newLocation.y, token.frame.origin.y + token.frame.size.height + topMargin, newLocation.x, token.center.x);
        if (tokenCaret.x + token.bounds.size.width + rightMargin > self.bounds.size.width)
        {
            _numberOfLines++;
            tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
            tokenCaret.y += lineHeight;
            CGPoint point = CGPointMake(self.frame.size.width - newLocation.x, newLocation.y - lineHeight);
            if (   point.y > token.frame.origin.y - topMargin
                && point.y < token.frame.origin.y + token.frame.size.height + topMargin
                && point.x > token.center.x)
                newIndex = i+1;
        }
        tokenCaret.x += token.bounds.size.width + 4;
        if (self.bounds.size.width - tokenCaret.x - rightMargin < 75)
        {
            _numberOfLines++;
            tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
            tokenCaret.y += lineHeight;
            CGPoint point = CGPointMake(self.frame.size.width - newLocation.x, newLocation.y - lineHeight);
            if (   point.y > token.frame.origin.y - topMargin
                && point.y < token.frame.origin.y + token.frame.size.height + topMargin
                && point.x > token.center.x)
                newIndex = i+1;
        }
        
        if (   newLocation.y > token.frame.origin.y - topMargin
            && newLocation.y < token.frame.origin.y + token.frame.size.height + topMargin
            && newLocation.x > token.center.x)
            newIndex = i+1;
        
        
    }
    
    int indexOfTokenBeingMoved = [self.tokens indexOfObject:tokenBeingMoved];
    if (newIndex == indexOfTokenBeingMoved)
    {
        if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
        {
            [self layoutTokensAnimated:YES];
        }
        return;
    }
    
    
    [self.tokens insertObject:tokenBeingMoved atIndex:newIndex];
    if (newIndex < indexOfTokenBeingMoved)
        [self.tokens removeObjectAtIndex:indexOfTokenBeingMoved+1];
    else
        [self.tokens removeObjectAtIndex:indexOfTokenBeingMoved];
    
    
    
    
    
    
    _numberOfLines = 1;
    
	tokenCaret = (CGPoint){leftMargin, (topMargin - 1)};
	
	[self.tokens enumerateObjectsUsingBlock:^(TIToken * token, NSUInteger idx, BOOL *stop){
        
		[token setFont:self.font];
		[token setMaxWidth:(self.bounds.size.width - rightMargin - (_numberOfLines > 1 ? hPadding : leftMargin))];
		
		if (token.superview)
        {
			
			if (tokenCaret.x + token.bounds.size.width + rightMargin > self.bounds.size.width)
            {
				_numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
			
            if (token != tokenBeingMoved)
                [token setFrame:(CGRect){tokenCaret, token.bounds.size}];
			tokenCaret.x += token.bounds.size.width + 4;
			if (self.bounds.size.width - tokenCaret.x - rightMargin < 75)
            {
				_numberOfLines++;
				tokenCaret.x = (_numberOfLines > 1 ? hPadding : leftMargin);
				tokenCaret.y += lineHeight;
			}
		}
	}];
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        
        [self layoutTokensAnimated:YES];
    }
    
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - View Handlers
/////////////////////////////////////////////////////////////////////////////////
- (void)layoutTokensAnimated:(BOOL)animated
{
	
	CGFloat newHeight = [self layoutTokensInternal];
	if (self.bounds.size.height != newHeight){
		
		// Animating this seems to invoke the triple-tap-delete-key-loop-problem-thing™
		[UIView animateWithDuration:(animated ? 0.3 : 0) animations:^{
			[self setFrame:((CGRect){self.frame.origin, {self.bounds.size.width, newHeight}})];
			[self sendActionsForControlEvents:TITokenFieldControlEventFrameWillChange];
			
		} completion:^(BOOL complete){
			if (complete) [self sendActionsForControlEvents:TITokenFieldControlEventFrameDidChange];
		}];
	}
}

/////////////////////////////////////////////////////////////////////////////////
- (void)setResultsModeEnabled:(BOOL)flag
{
	[self setResultsModeEnabled:flag animated:YES];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)setResultsModeEnabled:(BOOL)flag animated:(BOOL)animated
{
	
	[self layoutTokensAnimated:animated];
	
	if (_resultsModeEnabled != flag){
		
		//Hide / show the shadow
		[self.layer setMasksToBounds:!flag];
		
		UIScrollView * scrollView = self.scrollView;
		[scrollView setScrollsToTop:!flag];
		[scrollView setScrollEnabled:!flag];
		
		CGFloat offset = ((_numberOfLines == 1 || !flag) ? 0 : tokenCaret.y - floor(self.font.lineHeight * 4 / 7) + 1);
		[scrollView setContentOffset:CGPointMake(0, self.frame.origin.y + offset) animated:animated];
	}
	
	_resultsModeEnabled = flag;
}



/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Layout
/////////////////////////////////////////////////////////////////////////////////
- (CGRect)textRectForBounds:(CGRect)bounds
{
	
	if ([self.text isEqualToString:kTextHidden]) return CGRectMake(0, -20, 0, 0);
	
	CGRect frame = CGRectOffset(bounds, tokenCaret.x + 2, tokenCaret.y + 3);
	frame.size.width -= (tokenCaret.x + self.rightViewWidth + 10);
	
	return frame;
}

/////////////////////////////////////////////////////////////////////////////////
- (CGRect)editingRectForBounds:(CGRect)bounds
{
	return [self textRectForBounds:bounds];
}

/////////////////////////////////////////////////////////////////////////////////
- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
	return [self textRectForBounds:bounds];
}

/////////////////////////////////////////////////////////////////////////////////
- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
	return ((CGRect){{8, ceilf(self.font.lineHeight * 4 / 7)}, self.leftView.bounds.size});
}

/////////////////////////////////////////////////////////////////////////////////
- (CGRect)rightViewRectForBounds:(CGRect)bounds
{
	return ((CGRect){{bounds.size.width - self.rightView.bounds.size.width - 6,
		bounds.size.height - self.rightView.bounds.size.height - 6}, self.rightView.bounds.size});
}

/////////////////////////////////////////////////////////////////////////////////
- (CGFloat)leftViewWidth
{
	
	if (self.leftViewMode == UITextFieldViewModeNever ||
		(self.leftViewMode == UITextFieldViewModeUnlessEditing && self.editing) ||
		(self.leftViewMode == UITextFieldViewModeWhileEditing && !self.editing)) return 0;
	
	return self.leftView.bounds.size.width;
}

/////////////////////////////////////////////////////////////////////////////////
- (CGFloat)rightViewWidth
{
	
	if (self.rightViewMode == UITextFieldViewModeNever ||
		(self.rightViewMode == UITextFieldViewModeUnlessEditing && self.editing) ||
		(self.rightViewMode == UITextFieldViewModeWhileEditing && !self.editing)) return 0;
	
	return self.rightView.bounds.size.width;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Other
/////////////////////////////////////////////////////////////////////////////////
- (NSString *)description
{
	return [NSString stringWithFormat:@"<TITokenField %p; prompt = \"%@\">", self, ((UILabel *)self.leftView).text];
}

/////////////////////////////////////////////////////////////////////////////////
- (void)dealloc
{
	[self setDelegate:nil];
}

@end


@implementation TITokenFieldInternalDelegate
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate
/////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	
	if ([self.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)])
		return [self.delegate textFieldShouldBeginEditing:textField];
	
	return YES;
}

/////////////////////////////////////////////////////////////////////////////////
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	
	if ([self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
		[self.delegate textFieldDidBeginEditing:textField];
}

/////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
	
	if ([self.delegate respondsToSelector:@selector(textFieldShouldEndEditing:)])
		return [self.delegate textFieldShouldEndEditing:textField];
	
	return YES;
}

/////////////////////////////////////////////////////////////////////////////////
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if ([self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)])
		[self.delegate textFieldDidEndEditing:textField];
	
}

/////////////////////////////////////////////////////////////////////////////////
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	
	if (self.tokenField.tokens.count && [string isEqualToString:@""] && [self.tokenField.text isEqualToString:kTextEmpty])
    {
		[self.tokenField selectToken:[self.tokenField.tokens lastObject]];
		return NO;
	}
	
	if ([textField.text isEqualToString:kTextHidden])
    {
		[self.tokenField removeToken:self.tokenField.selectedToken];
		return (![string isEqualToString:@""]);
	}
	
	if ([string rangeOfCharacterFromSet:self.tokenField.tokenizingCharacters].location != NSNotFound)
    {
		[self.tokenField tokenizeText];
		return NO;
	}
	
	if ([self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
		return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
	
	
	return YES;
}

/////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.tokenField tokenizeText];
	if ([self.delegate respondsToSelector:@selector(textFieldShouldReturn:)])
		return [self.delegate textFieldShouldReturn:textField];
	
	return YES;
}

/////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	if ([self.delegate respondsToSelector:@selector(textFieldShouldClear:)])
		return [self.delegate textFieldShouldClear:textField];
		
	return YES;
}

@end


