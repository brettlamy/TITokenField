//
//  TokenFieldExampleViewController.m
//  TokenFieldExample
//
//  Created by Tom Irving on 29/01/2011.
//  Copyright 2011 Tom Irving. All rights reserved.
//

#import "TokenFieldExampleViewController.h"
#import "TIToken.h"
@interface TokenFieldExampleViewController() <TITokenFieldDelegate, UITextViewDelegate>
@property (nonatomic, strong) TITokenField * tokenFieldView;
@end

@implementation TokenFieldExampleViewController
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#pragma mark - View Life cycle
/////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
	[self.view setBackgroundColor:[UIColor whiteColor]];
	[self.navigationItem setTitle:@"Example"];
	self.tokenFieldView = [[TITokenField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
	[self.view addSubview:self.tokenFieldView];
	[self.tokenFieldView setDelegate:self];
	[self.tokenFieldView setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;. "]]; // Default is a comma
	[self.tokenFieldView becomeFirstResponder];
}

@end